///1.Опишіть своїми словами, що таке метод об'єкту
///
/// Метод об'єкту це функція яка належить об'єкту.

///2.Який тип даних може мати значення властивості об'єкта?
///
///Властивість об'єкта це значення будь якого типу яке належить об'єкту.

///3.Об'єкт це посилальний тип даних. Що означає це поняття?
///
/// Це означає, що змінна, в яку присвоєний об'єкт, буде зберігати не сам об'єкт,
// а посилання на нього.





function createNewUser () {

  const firstName = prompt("Введіть Ваше ім'я");
  while (firstName === null || firstName === "" || !isNaN(firstName)) {
    firstName = prompt("Введіть Ваше ім'я", firstName);
  }

  const lastName = prompt("Введіть Ваше прізвище");
  while (lastName === null || lastName === "" || !isNaN(lastName)) {
    lastName = prompt("Введіть Ваше прізвище", lastName);
  }

  let newUser = {
    firstName: firstName,
    lastName: lastName,
  };
  function getLogin() {
    return (newUser.firstName[0] + newUser.lastName).toLowerCase();
  }
  return console.log(getLogin());
}
createNewUser();